//
//  ResultType.swift
//  Quizer
//
//  Created by Peter Bruz on 03/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import Foundation

enum ResultType<T> {
    case success(T)
    case failure(Error)
}
