//
//  QuizTableViewCellProtocol.swift
//  Quizer
//
//  Created by Peter Bruz on 06/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import UIKit

protocol QuizTableViewCellProtocol {

    var lastResult: String? { get set }
    var progress: String? { get set }

    var titleLabel: UILabel { get }
    var backgroundImageView: UIImageView { get }
}
