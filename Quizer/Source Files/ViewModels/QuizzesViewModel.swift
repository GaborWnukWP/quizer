//
//  QuizzesViewModel.swift
//  Quizer
//
//  Created by Peter Bruz on 03/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import Foundation

final class QuizzesViewModel {

    fileprivate lazy var quizProvider = QuizProvider()
    fileprivate(set) var quizzes: [QuizDescription]  = []
}

// MARK: Public methods

extension QuizzesViewModel {

    /// Provides quizzes.
    ///
    /// - Parameter completion: Closure that is called after fetching quizzes.
    ///                         If providing succeeds, ResultType is `success`,
    ///                         otherwise it's `failed` and contains error.
    func provideQuizzes(completion: @escaping (ResultType<Void>) -> Void) {

        quizProvider.provideQuizDescriptions { [weak self] result in
            switch result {
            case .success(let quizzes):
                self?.quizzes = quizzes
                self?.provideQuizStatusList(completion: completion)
            case .failure(let error): completion(ResultType.failure(error))
            }
        }
    }

    /// Updates quiz with given identifier by setting new status.
    ///
    /// - Parameters:
    ///   - id: Identifier of quiz to update.
    ///   - status: Status to update quiz with.
    func updateQuiz(withId id: String, withStatus status: QuizStatus) {
        if let index = indexOfQuiz(withId: id) {
            var quiz = quizzes[index]
            quiz.quizStatus = status
            quizzes[index] = quiz
        }
    }

    /// Returns index of quiz with given identifier.
    ///
    /// - Parameter id: Identifier of quiz.
    /// - Returns: Index of quiz. If not found, than returns nil.
    func indexOfQuiz(withId id: String) -> Int? {
        return quizzes.index(where: { $0.id == id })
    }
}

// MARK: Private

private extension QuizzesViewModel{

    func provideQuizStatusList(completion: @escaping (ResultType<Void>) -> Void) {
        quizProvider.provideQuizStatusList { [weak self] result in
            switch result {
            case .success(let quizStatusList):
                self?.assignStatuses(quizStatusList: quizStatusList)
                completion(ResultType.success())
            case .failure(let error): completion(ResultType.failure(error))
            }
        }
    }

    func assignStatuses(quizStatusList: [QuizStatus]) {
        quizStatusList.forEach { quizStatus in
            if let index = quizzes.index(where: { $0.id == quizStatus.quizId }) {
                quizzes[index].quizStatus = quizStatus
            }
        }
    }
}
