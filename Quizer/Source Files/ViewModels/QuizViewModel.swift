//
//  QuizViewModel.swift
//  Quizer
//
//  Created by Peter Bruz on 03/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import Foundation

final class QuizViewModel {

    enum QuizError: Error {
        case notFinished
        case alreadyFinished
    }

    /// Indicates if quiz has next question.
    var hasNextQuestion: Bool {
        return quizDetails.questions.count > currentQuestionOrder
    }

    /// Indicates if quiz is on last question
    var isLastQuestion: Bool {
        return quizDetails.questions.count == currentQuestionOrder
    }

    /// Result of quiz. Should be called after finishing quiz.
    var quizResult: (rateDescription:String, percentage: String) {
        let result = Float(correctAnswers)/Float(quizDetails.questions.count) * 100
        let percentage = String(format: "%0.f%%", result)
        let rateDescription = quizDetails.rates.first(where: { $0.concerns(result: Int(result)) })?.content ?? ""
        return (rateDescription, percentage)
    }

    /// Order of currently displayed question.
    var currentQuestionOrder: Int = 0

    fileprivate var correctAnswers = 0

    fileprivate(set) var quizDetails = QuizDetails(rates: [], questions: [])
    fileprivate lazy var quizProvider = QuizProvider()
    fileprivate lazy var quizRequester = QuizRequester()

    fileprivate(set) var quiz: QuizDescription

    /// Inits view model with quiz.
    ///
    /// - Parameter quiz: Quiz to init view model with
    init(quiz: QuizDescription) {
        self.quiz = quiz
    }
}

// MARK: Public methods

extension QuizViewModel {

    /// Provides details for current quiz.
    ///
    /// - Parameter completion: Closure that is called after providing quiz details.
    ///                         If Providing succeeds, ResultType is `success`,
    ///                         otherwise it's `failed` and contains error.
    func provideQuizDetails(completion: @escaping (ResultType<Void>) -> Void) {
        quizProvider.provideQuizDetails(withId: quiz.id) { [weak self] result in
            switch result {
            case .success(let quizDetails):
                self?.quizDetails = quizDetails
                self?.currentQuestionOrder = quizDetails.questions.isEmpty ? 0 : 1

                if let status = self?.quiz.quizStatus, status.percentageProgress != nil {
                    self?.currentQuestionOrder = status.lastQuestionOrder
                }
                completion(ResultType.success())
            case .failure(let error): completion(ResultType.failure(error))
            }
        }
    }

    /// Handles selection of order.
    ///
    /// - Parameters:
    ///   - answerOrder: Order of selected answer.
    ///   - questionOrder: Order of selected question.
    func handleAnswer(withOrder answerOrder: Int, forQuestionWithOrder questionOrder: Int) {

        guard
            let question = quizDetails.questions.first(where: { $0.order == questionOrder }),
            let answer = question.answers.first(where: { $0.order == answerOrder }),
            answer.isCorrect
        else {
            return
        }

        correctAnswers += 1
    }

    /// Saves result of quiz. Call this method only if you finished the quiz.
    ///
    /// - Parameter completion: Closure that is called after saving result.
    ///                         If saving succeeds, ResultType is `success`,
    ///                         otherwise it's `failed` and contains error.
    func saveResult(completion: @escaping (ResultType<Void>) -> Void) {

        guard !hasNextQuestion else {
            completion(ResultType.failure(QuizError.notFinished))
            return
        }

        let quizStatus = prepareQuizStatusForFinalResult()

        save(quizStatus: quizStatus, completion: completion)
    }

    /// Saves quiz progress. Call this method only when you are in progress of quiz.
    ///
    /// - Parameter completion: Closure that is called after saving progress.
    ///                         If saving succeeds, ResultType is `success`,
    ///                         otherwise it's `failed` and contains error.
    func saveCurrentProgress(completion: @escaping (ResultType<Void>) -> Void) {

        guard hasNextQuestion || isLastQuestion else {
            completion(ResultType.failure(QuizError.alreadyFinished))
            return
        }

        guard currentQuestionOrder > 1 else {
            completion(ResultType.success())
            return
        }

        let quizStatus = prepareQuizStatusForCurrentProgress()

        save(quizStatus: quizStatus, completion: completion)
    }

    /// Prepares class for quiz repeating.
    func prepareForRepeating() {
        correctAnswers = 0
        currentQuestionOrder = 1
    }
}

// MARK: Private

private extension QuizViewModel {

    func save(quizStatus: QuizStatus, completion: @escaping (ResultType<Void>) -> Void) {
        quizRequester.save(quizStatus: quizStatus) { [weak self] result in
            switch result {
            case .success:
                self?.quiz.quizStatus = quizStatus
                completion(ResultType.success())
            case .failure(let error): completion(ResultType.failure(error))
            }
        }
    }

    func prepareQuizStatusForFinalResult() -> QuizStatus {

        let lastQuestionOrder: Int = 0
        let percentageProgress: String? = nil
        let lastResult = "\(correctAnswers)/\(quizDetails.questions.count)"
        let lastPercentageResult = quizResult.percentage

        let quizStatus: QuizStatus
        if var status = quiz.quizStatus {
            status.lastQuestionOrder = lastQuestionOrder
            status.percentageProgress = percentageProgress
            status.lastResult = lastResult
            status.lastPercentageResult = lastPercentageResult
            quizStatus = status
        } else {
            quizStatus = QuizStatus(
                quizId: quiz.id,
                lastQuestionOrder: lastQuestionOrder,
                percentageProgress: percentageProgress,
                lastResult: lastResult,
                lastPercentageResult: lastPercentageResult
            )
        }

        return quizStatus
    }

    func prepareQuizStatusForCurrentProgress() -> QuizStatus {

        let lastQuestionOrder = currentQuestionOrder
        let progress = Float(currentQuestionOrder - 1)/Float(quizDetails.questions.count) * 100
        let percentageProgress = String(format: "%0.f%%", progress)

        let quizStatus: QuizStatus
        if var status = quiz.quizStatus {
            status.lastQuestionOrder = lastQuestionOrder
            status.percentageProgress = percentageProgress
            quizStatus = status
        } else {
            quizStatus = QuizStatus(
                quizId: quiz.id,
                lastQuestionOrder: lastQuestionOrder,
                percentageProgress: percentageProgress,
                lastResult: nil,
                lastPercentageResult: nil
            )
        }

        return quizStatus
    }
}
