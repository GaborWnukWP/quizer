//
//  DatabaseQuizStatusRequester.swift
//  Quizer
//
//  Created by Peter Bruz on 05/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import Foundation

final class DatabaseQuizStatusRequester {

    private lazy var managedQuizStatusRequester = ManagedQuizStatusRequester()

    /// Saves quiz status to database.
    ///
    /// - Parameters:
    ///   - quizStatus: Quiz status to save.
    ///   - completion: Closure that is called after saving status.
    ///                 If saving succeeds, ResultType is `success`, otherwise it's `failed` and contains error.
    func save(quizStatus: QuizStatus, completion: (ResultType<Void>) -> Void) {
        do {
            try managedQuizStatusRequester.save(quizStatus: quizStatus)
            completion(ResultType.success())
        } catch let error {
            completion(ResultType.failure(error))
        }
    }
}
