//
//  SummaryView.swift
//  Quizer
//
//  Created by Peter Bruz on 04/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import UIKit
import EasyPeasy

final class SummaryView: UIView {

    fileprivate enum StaticText: String, Localizable {
        case youGot = "SummaryView/you-got"
        case correctAnswers = "SummaryView/correct-answers"
        case tryAgain = "SummaryView/try-again"
        case goToQuizList = "SummaryView/go-to-quiz-list"
    }

    let titleLabel = UILabel()
    let resultLabel = UILabel()
    let quizzesButton = UIButton()
    let repeatButton = UIButton()

    fileprivate let preResultLabel = UILabel()
    fileprivate let postResultLabel = UILabel()
    fileprivate let centeredContentView = UIView()

    // MARK: Life Cycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupHierarchy()
        setupInitialConstraints()
    }

    @available(*, unavailable, message: "Use init(frame:) instead")
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: Private

private extension SummaryView {

    func setupViews() {

        isHidden = true
        backgroundColor = UIColor.black.withAlphaComponent(0.85)

        centeredContentView.backgroundColor = .white
        centeredContentView.layer.cornerRadius = 3

        titleLabel.textColor = .appLightBlue
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.systemFont(ofSize: 21)

        preResultLabel.text = StaticText.youGot.localized
        preResultLabel.textColor = .black
        preResultLabel.textAlignment = .center

        resultLabel.textColor = .red
        resultLabel.textAlignment = .center
        resultLabel.font = UIFont.systemFont(ofSize: 28)

        postResultLabel.text = StaticText.correctAnswers.localized
        postResultLabel.textColor = .black
        postResultLabel.textAlignment = .center

        repeatButton.setTitle(StaticText.tryAgain.localized, for: .normal)
        repeatButton.setTitleColor(.white, for: .normal)
        repeatButton.backgroundColor = .appLightBlue
        repeatButton.layer.cornerRadius = 3
        repeatButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)

        quizzesButton.setTitle(StaticText.goToQuizList.localized, for: .normal)
        quizzesButton.setTitleColor(.white, for: .normal)
        quizzesButton.backgroundColor = .appLightBlue
        quizzesButton.layer.cornerRadius = 3
        quizzesButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
    }

    func setupHierarchy() {
        centeredContentView.add(subviews: titleLabel, preResultLabel, resultLabel, postResultLabel, repeatButton, quizzesButton)
        add(subviews: centeredContentView)
    }

    func setupInitialConstraints() {

        centeredContentView <- [
            Leading(30), Trailing(30),
            CenterY()
        ]

        titleLabel <- [
            Leading(30), Trailing(30),
            Top(30)
        ]

        preResultLabel <- [
            Leading(30), Trailing(30),
            Top(20).to(titleLabel, .bottom)
        ]

        resultLabel <- [
            Leading(30), Trailing(30),
            Top(20).to(preResultLabel, .bottom)
        ]

        postResultLabel <- [
            Leading(30), Trailing(30),
            Top(20).to(resultLabel, .bottom)
        ]

        repeatButton <- [
            Leading(30), Trailing(30),
            Top(20).to(postResultLabel, .bottom)
        ]

        quizzesButton <- [
            Leading(30), Trailing(30),
            Top(10).to(repeatButton, .bottom), Bottom(30)
        ]
    }
}
