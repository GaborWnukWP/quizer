//
//  RadioButtonView.swift
//  Quizer
//
//  Created by Peter Bruz on 04/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import UIKit
import EasyPeasy

final class RadioButtonView: UIView {

    var selected = false {
        didSet {
            setupViewSelected(selected)
        }
    }

    fileprivate let borderView = UIView()
    fileprivate let fillingView = UIView()

    fileprivate let borderViewSize = CGSize(width: 15, height: 15)
    fileprivate let fillingRatio = CGFloat(0.4)

    // MARK: Life Cycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupHierarchy()
        setupInitialConstraints()
    }

    @available(*, unavailable, message: "Use init(frame:) instead")
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension RadioButtonView {

    func setupViews() {

        borderView.backgroundColor = .white
        borderView.layer.cornerRadius = borderViewSize.height / 2
        borderView.layer.borderWidth = 1
        borderView.layer.borderColor = UIColor.appDarkBlue.cgColor
        borderView.clipsToBounds = true

        fillingView.backgroundColor = .appLightBlue
        fillingView.layer.cornerRadius = borderViewSize.height * fillingRatio / 2
        fillingView.clipsToBounds = true

        setupViewSelected(false)
    }

    func setupHierarchy() {
        add(subviews: borderView)
        borderView.add(subviews:fillingView)
    }

    func setupInitialConstraints() {

        borderView <- [
            CenterX(), Width(borderViewSize.width),
            CenterY(), Height(borderViewSize.height)
        ]

        fillingView <- [
            CenterX(), Width(*fillingRatio).like(borderView),
            CenterY(), Height(*fillingRatio).like(borderView)
        ]
    }

    func setupViewSelected(_ selected: Bool) {

        fillingView.isHidden = !selected
    }
}
