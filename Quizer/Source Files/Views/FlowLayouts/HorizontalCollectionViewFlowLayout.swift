//
//  HorizontalCollectionViewFlowLayout.swift
//  Quizer
//
//  Created by Peter Bruz on 04/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import UIKit

final class HorizontalCollectionViewFlowLayout: UICollectionViewFlowLayout {

    override func prepare() {

        if let collectionView = collectionView {
            itemSize = collectionView.frame.size
            minimumLineSpacing = 0
            minimumInteritemSpacing = 0
            scrollDirection = .horizontal
        }
    }

    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
}
