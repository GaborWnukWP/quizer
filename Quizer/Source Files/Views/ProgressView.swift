//
//  ProgressView.swift
//  Quizer
//
//  Created by Peter Bruz on 04/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import UIKit
import EasyPeasy

final class ProgressView: UIView {

    fileprivate let fillingView = UIView()

    // MARK: Life Cycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupHierarchy()
        setupInitialConstraints()
    }

    @available(*, unavailable, message: "Use init(frame:) instead")
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: Public

extension ProgressView {

    /// Sets progress for progress view.
    ///
    /// - Parameters:
    ///   - progress: Progress to show.
    ///   - animated: Indicates if animation should be performed
    func setProgress(progress: CGFloat, animated: Bool = false) {

        fillingView <- Width(frame.width * progress)
        if animated {
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations: {
                self.layoutIfNeeded()
            }, completion: nil)
        }
    }
}

private extension ProgressView {

    func setupViews() {

        backgroundColor = UIColor.appLightBlue.withAlphaComponent(0.5)
        clipsToBounds = true

        layer.borderColor = UIColor.black.cgColor
        layer.cornerRadius = 2
        layer.borderWidth = 1

        fillingView.backgroundColor = .appLightBlue
    }

    func setupHierarchy() {
        add(subviews: fillingView)
    }

    func setupInitialConstraints() {

        fillingView <- [
            Leading(), Width(0),
            Top(), Bottom(),
        ]
    }
}
