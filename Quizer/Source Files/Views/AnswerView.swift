//
//  AnswerView.swift
//  Quizer
//
//  Created by Peter Bruz on 04/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import UIKit
import EasyPeasy

final class AnswerView: UIView {

    var didTap: (() -> Void)?

    var selected = false {
        didSet {
            setupViewSelected(selected)
        }
    }

    var selectable = true

    let answerTextLabel = UILabel()

    fileprivate let radioButtonView = RadioButtonView()

    fileprivate lazy var tapGestureRecognizer: UITapGestureRecognizer = { [unowned self] in
        let gestureRecognizer = UITapGestureRecognizer()
        gestureRecognizer.addTarget(self, action: #selector(didTap(_:)))
        return gestureRecognizer
    }()

    // MARK: Life Cycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupHierarchy()
        setupInitialConstraints()
    }

    @available(*, unavailable, message: "Use init(frame:) instead")
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension AnswerView {

    func setupViews() {

        addGestureRecognizer(tapGestureRecognizer)

        layer.borderWidth = 1
        layer.cornerRadius = 3
        layer.borderColor = UIColor.appDarkBlue.cgColor

        answerTextLabel.numberOfLines = 0
        answerTextLabel.font = UIFont.systemFont(ofSize: 14)
        answerTextLabel.textColor = .appDarkBlue

        setupViewSelected(false)
    }

    func setupHierarchy() {
        add(subviews: radioButtonView, answerTextLabel)
    }

    func setupViewSelected(_ selected: Bool) {

        radioButtonView.selected = selected

        backgroundColor = selected ? .appLightBlue : .white
        answerTextLabel.textColor = selected ? .white : .appDarkBlue
    }

    func setupInitialConstraints() {

        radioButtonView <- [
            Leading(8), Width(15),
            CenterY().to(answerTextLabel), Height(15)
        ]

        answerTextLabel <- [
            Left(8).to(radioButtonView, .right), Trailing(8),
            Top(8), Bottom(8)
        ]
    }
    
    dynamic func didTap(_: UITapGestureRecognizer) {
        guard selectable else { return }

        setupViewSelected(true)
        didTap?()
    }
}
