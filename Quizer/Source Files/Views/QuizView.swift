//
//  QuizView.swift
//  Quizer
//
//  Created by Peter Bruz on 03/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import UIKit
import EasyPeasy

final class QuizView: UIView {

    fileprivate enum StaticText: String, Localizable {
        case stop = "QuizView/stop"
    }

    let backgroundImageView = UIImageView()
    let stopButton = UIButton()
    let collectionView = UICollectionView(frame: .zero, collectionViewLayout: HorizontalCollectionViewFlowLayout())
    let progressView = ProgressView()
    let summaryView = SummaryView()
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)

    fileprivate let dimView = UIView()
    fileprivate let stopButtonContainerView = UIView()

    // MARK: Life Cycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupHierarchy()
        setupInitialConstraints()
    }

    @available(*, unavailable, message: "Use init(frame:) instead")
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: Public methods

extension QuizView {

    /// Shows summary view.
    ///
    /// - Parameter animated: Indicates if showing should be animated.
    func showSummaryView(animated: Bool) {
        if animated {
            summaryView.alpha = 0
            summaryView.isHidden = false
            UIView.animate(withDuration: 0.2, delay: 0.2, options: .curveEaseOut, animations: { 
                self.summaryView.alpha = 1
            }, completion: nil)
        } else {
            summaryView.isHidden = false
        }
    }
}

// MARK: Private

private extension QuizView {

    func setupViews() {
        backgroundColor = .white

        backgroundImageView.contentMode = .scaleAspectFill
        backgroundImageView.isUserInteractionEnabled = true

        dimView.backgroundColor = UIColor.darkGray.withAlphaComponent(0.8)

        activityIndicator.startAnimating()

        collectionView.backgroundColor = .clear
        collectionView.isPagingEnabled = true
        collectionView.bounces = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.isScrollEnabled = false

        stopButtonContainerView.backgroundColor = UIColor.red.withAlphaComponent(0.6)
        stopButtonContainerView.layer.cornerRadius = 3

        stopButton.setTitle(StaticText.stop.localized, for: .normal)
        stopButton.setTitleColor(.white, for: .normal)
        stopButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
    }

    func setupHierarchy() {
        add(subviews:backgroundImageView)
        backgroundImageView.add(subviews: dimView)
        stopButtonContainerView.add(subviews: stopButton)
        dimView.add(subviews: collectionView, progressView, stopButtonContainerView)
        dimView.add(subviews: summaryView)
        add(subviews: activityIndicator)
    }

    func setupInitialConstraints() {

        backgroundImageView <- Edges()

        dimView <- Edges()

        collectionView <- [
            Leading(), Trailing(),
            Top(), Bottom().to(stopButton, .top)
        ]

        progressView <- [
            Leading(20), Right(20).to(stopButtonContainerView, .left),
            CenterY().to(stopButtonContainerView), Height(20)
        ]

        stopButtonContainerView <- [
            Trailing(10),
            Bottom(10)
        ]

        stopButton <- [
            Leading(7), Trailing(7),
            Top(), Bottom()
        ]

        summaryView <- Edges()

        activityIndicator <- Center()
    }
}
