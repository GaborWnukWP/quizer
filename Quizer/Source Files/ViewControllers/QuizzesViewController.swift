//
//  QuizzesViewController.swift
//  Quizer
//
//  Created by Peter Bruz on 03/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import UIKit
import SDWebImage

final class QuizzesViewController: UIViewController {

    fileprivate lazy var quizzesView: QuizzesView = QuizzesView()

    fileprivate let viewModel = QuizzesViewModel()

    override func loadView() {
        view = quizzesView
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()

        fetchQuizzes()
    }
}

extension QuizzesViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.quizzes.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: QuizTableViewCellProtocol = {
            if UIDevice.current.userInterfaceIdiom == .pad {
                return tableView.dequeueReusableCell(withIdentifier: QuizTableViewCell_iPad.reuseIdentifier) as! QuizTableViewCell_iPad
            } else {
                return tableView.dequeueReusableCell(withIdentifier: QuizTableViewCell.reuseIdentifier) as! QuizTableViewCell
            }
        }()
        let quiz = viewModel.quizzes[indexPath.row]
        cell.titleLabel.text = quiz.title
        cell.backgroundImageView.sd_setImage(with: quiz.photoUrl)
        cell.lastResult = quiz.quizStatus?.lastResult
        cell.progress = quiz.quizStatus?.percentageProgress
        return cell as! UITableViewCell
    }
}

extension QuizzesViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // http://stackoverflow.com/questions/21075540/presentviewcontrolleranimatedyes-view-will-not-appear-until-user-taps-again  -  Apple, why?! :(
        DispatchQueue.main.async { [unowned self] in
            let quizViewController = QuizViewController(quiz: self.viewModel.quizzes[indexPath.row])
            quizViewController.modalTransitionStyle = .crossDissolve
            quizViewController.modalPresentationStyle = .overFullScreen
            quizViewController.delegate = self
            self.present(quizViewController, animated: true)
        }
    }
}

extension QuizzesViewController: QuizViewControllerDelegate {

    func viewController(_ viewController: QuizViewController, didUpdateQuizWithId id: String, withQuizStatus quizStatus: QuizStatus) {
        viewModel.updateQuiz(withId: id, withStatus: quizStatus)
        if let index = viewModel.indexOfQuiz(withId: id) {
            quizzesView.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
        }
    }
}

private extension QuizzesViewController {

    func setupViews() {

        quizzesView.tableView.dataSource = self
        quizzesView.tableView.delegate = self
        quizzesView.tableView.register(QuizTableViewCell.self, forCellReuseIdentifier: QuizTableViewCell.reuseIdentifier)
        quizzesView.tableView.register(QuizTableViewCell_iPad.self, forCellReuseIdentifier: QuizTableViewCell_iPad.reuseIdentifier)
    }

    func fetchQuizzes() {
        viewModel.provideQuizzes { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success: self?.quizzesView.tableView.reloadData()
                case .failure: break
                }
                self?.quizzesView.activityIndicator.stopAnimating()
            }
        }
    }
}
