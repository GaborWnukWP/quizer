//
//  QuizViewController.swift
//  Quizer
//
//  Created by Peter Bruz on 03/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import UIKit
import SDWebImage

protocol QuizViewControllerDelegate: class {


    /// Method that is called when view controller updates it's quiz with new status.
    ///
    /// - Parameters:
    ///   - viewController: View controller that calls method.
    ///   - id: Identifier of updated quiz.
    ///   - quizStatus: Status that quiz has been updated with.
    func viewController(_ viewController: QuizViewController, didUpdateQuizWithId id: String, withQuizStatus quizStatus: QuizStatus)
}

final class QuizViewController: UIViewController {

    /// Delegate object that conforms to `QuizViewControllerDelegate` protocol.
    weak var delegate: QuizViewControllerDelegate?

    fileprivate lazy var quizView: QuizView = QuizView()

    fileprivate let viewModel: QuizViewModel

    /// Initializes view controller with quiz.
    ///
    /// - Parameter quiz: Quiz to initialize view controller with.
    init(quiz: QuizDescription) {
        viewModel = QuizViewModel(quiz: quiz)
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable, message: "Use init(quiz:) method instead")
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        view = quizView
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()

        fetchQuizDetails()
    }
}

// MARK: UICollectionViewDataSource

extension QuizViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.quizDetails.questions.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: QuizQuestionCollectionViewCell.reuseIdentifier, for: indexPath) as! QuizQuestionCollectionViewCell

        let question = viewModel.quizDetails.questions[indexPath.item]

        cell.questionLabel.text = question.text
        cell.answers = question.answers.map { ($0.order, $0.text) }
        cell.imageUrl = question.imageUrl
        cell.didSelectAnswerWithOrder = { [weak self] order in
            self?.handleAnswer(withOrder: order, forQuestionWithOrder: question.order)
        }

        return cell
    }
}

// MARK: Private

private extension QuizViewController {

    func setupViews() {

        quizView.backgroundImageView.sd_setImage(with: viewModel.quiz.photoUrl)
        quizView.collectionView.dataSource = self
        quizView.collectionView.register(QuizQuestionCollectionViewCell.self, forCellWithReuseIdentifier: QuizQuestionCollectionViewCell.reuseIdentifier)
        quizView.stopButton.addTarget(self, action: #selector(didTapStopButton(_:)), for: .touchUpInside)
        quizView.summaryView.repeatButton.addTarget(self, action: #selector(didTapRepeatButton(_:)), for: .touchUpInside)
        quizView.summaryView.quizzesButton.addTarget(self, action: #selector(didTapBackToQuizzesButton(_:)), for: .touchUpInside)
    }

    func fetchQuizDetails() {
        viewModel.provideQuizDetails { [weak self] result in
            DispatchQueue.main.async {

                guard let certainSelf = self else { return }

                switch result {
                case .success:
                    certainSelf.quizView.collectionView.reloadData()

                    let questionOrder = certainSelf.viewModel.currentQuestionOrder
                    let questionCount = certainSelf.viewModel.quizDetails.questions.count
                    certainSelf.quizView.collectionView.scrollToItem(at: IndexPath(item: questionOrder - 1, section: 0), at: .centeredHorizontally, animated: false)
                    certainSelf.quizView.progressView.setProgress(progress: CGFloat(questionOrder - 1) / CGFloat(questionCount))
                case .failure: break
                }

                certainSelf.quizView.activityIndicator.stopAnimating()
            }
        }
    }

    dynamic func didTapStopButton(_: UIButton) {
        saveProgressAndDismiss()
    }

    dynamic func didTapBackToQuizzesButton(_:UIButton) {
        dismiss(animated: true, completion: nil)
    }

    dynamic func didTapRepeatButton(_: UIButton) {
        viewModel.prepareForRepeating()

        quizView.collectionView.reloadData()
        quizView.collectionView.scrollToItem(at: IndexPath(item: viewModel.currentQuestionOrder - 1, section: 0), at: .centeredHorizontally, animated: false)
        quizView.progressView.setProgress(progress: 0)
        quizView.summaryView.isHidden = true
    }

    func handleAnswer(withOrder answerOrder: Int, forQuestionWithOrder questionOrder: Int) {

        let questionCount = viewModel.quizDetails.questions.count

        viewModel.handleAnswer(withOrder: answerOrder, forQuestionWithOrder: questionOrder)

        quizView.progressView.setProgress(progress: CGFloat(viewModel.currentQuestionOrder) / CGFloat(questionCount), animated: true)

        if viewModel.hasNextQuestion {

            viewModel.currentQuestionOrder += 1
            quizView.collectionView.scrollToItem(at: IndexPath(item: viewModel.currentQuestionOrder - 1, section: 0), at: .centeredHorizontally, animated: true)
        } else {
            viewModel.saveResult() { [weak self] result in
                switch result {
                case .success:
                    self?.notifyDelegateAboutQuizStatusUpdate()
                    self?.proceedWithSummaryView()
                case .failure: break
                }
            }
        }
    }

    func proceedWithSummaryView() {
        let quizResult = viewModel.quizResult
        quizView.summaryView.titleLabel.text = quizResult.rateDescription
        quizView.summaryView.resultLabel.text = quizResult.percentage
        quizView.showSummaryView(animated: true)
    }

    func saveProgressAndDismiss() {
        viewModel.saveCurrentProgress() { [weak self] result in
            switch result {
            case .success: self?.notifyDelegateAboutQuizStatusUpdate()
            case .failure: break
            }
            self?.dismiss(animated: true, completion: nil)
        }
    }

    func notifyDelegateAboutQuizStatusUpdate() {
        guard let quizStatus = viewModel.quiz.quizStatus else { return }

        delegate?.viewController(self, didUpdateQuizWithId: viewModel.quiz.id, withQuizStatus: quizStatus)
    }
}
