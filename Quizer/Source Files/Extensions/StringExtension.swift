//
//  StringExtension.swift
//  Quizer
//
//  Created by Peter Bruz on 06/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import Foundation

extension String {

    /// Returns localized string based on given key.
    ///
    /// - Parameter key: Key for localized string.
    /// - Returns: Localized string based on given key
    static func localized(withKey key: String) -> String {
        return NSLocalizedString(key, comment: "")
    }
}
