//
//  UIColorExtension.swift
//  Quizer
//
//  Created by Peter Bruz on 04/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import UIKit

extension UIColor {

    class var appDarkBlue: UIColor {
        return UIColor.hex(0x094985)
    }

    class var appLightBlue: UIColor {
        return UIColor.hex(0x1e7fae)
    }

    /// Returns `UIColor` created with RGBA.
    ///
    /// - Parameters:
    ///   - red: Red value.
    ///   - green: Green value.
    ///   - blue: Blue value.
    ///   - alpha: Alpha value.
    /// - Returns: `UIColor` created with RGBA.
    class func rgba(red: Int, green: Int, blue: Int, alpha: CGFloat) -> UIColor {
        return UIColor(red: CGFloat(red)/255.0, green: CGFloat(green)/255.0, blue: CGFloat(blue)/255.0, alpha: alpha)
    }

    /// Returns `UIColor` created with RGB. Alpha is set to 1.
    ///
    /// - Parameters:
    ///   - red: Red value.
    ///   - green: Green value.
    ///   - blue: Blue value.
    /// - Returns: `UIColor` created with RGB.
    class func rgb(red: Int, green: Int, blue: Int) -> UIColor {
        return rgba(red: red, green: green, blue: blue, alpha: 1)
    }

    /// Returns `UIColor` created with HEX value.
    ///
    /// - Parameter hex: HEX value of color.
    /// - Returns: `UIColor` created with HEX value.
    class func hex(_ hex: Int) -> UIColor {

        let colorComponents = getColorComponents(from: hex)
        return rgb(red: colorComponents.red, green: colorComponents.green, blue: colorComponents.blue)
    }
}

private extension UIColor {

    class func getColorComponents(from hex: Int) -> (red: Int, green: Int, blue: Int) {
        let red = hex >> 16 & 0xFF
        let green = hex >> 8 & 0xFF
        let blue = hex & 0xFF

        return (red, green, blue)
    }
}
