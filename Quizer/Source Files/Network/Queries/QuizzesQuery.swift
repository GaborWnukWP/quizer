//
//  QuizzesQuery.swift
//  Quizer
//
//  Created by Peter Bruz on 03/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import Foundation

/// Query to get list of quizzes.
struct QuizzesQuery: Query {

    let method = Method.GET
    let path = "/quizzes/0/100"
}
