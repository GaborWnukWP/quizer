//
//  QuizQuery.swift
//  Quizer
//
//  Created by Peter Bruz on 03/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import Foundation

/// Query to get quiz in details.
struct QuizQuery: Query {

    let method = Method.GET
    let path: String

    /// Initializes query to get quiz in details.
    ///
    /// - Parameter id: Identifier of quiz to get.
    init(id: String) {
        path = "/quiz/\(id)/0"
    }
}
