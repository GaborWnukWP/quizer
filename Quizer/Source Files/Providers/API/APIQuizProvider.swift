//
//  APIQuizProvider.swift
//  Quizer
//
//  Created by Peter Bruz on 04/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import Foundation

final class APIQuizProvider {

    /// Provides list of quizzes from API.
    ///
    /// - Parameter completion: Closure that is called after fetching quizzes.
    ///                         If providing succeeds, ResultType is `success` and contains list of objects,
    ///                         otherwise it's `failed` and contains error.
    func provideQuizDescriptions(completion: @escaping (ResultType<[QuizDescription]>) -> Void) {
        Request(query: QuizzesQuery()).invoke { result in
            switch result {
            case .success(let quizzesJson):
                do {
                    let quizzes = try QuizDescriptionMapper().mapToQuizDescriptionList(json: quizzesJson)
                    completion(ResultType.success(quizzes))
                } catch let error {
                    completion(ResultType.failure(error))
                }
            case .failure(let error): completion(ResultType.failure(error))
            }
        }
    }

    /// Provides details of quiz with given identifier from API.
    ///
    /// - Parameters:
    ///   - id: Identifier of quiz to provide details for.
    ///   - completion: Closure that is called after fetching quiz details.
    ///                 If providing succeeds, ResultType is `success` and contains objects,
    ///                 otherwise it's `failed` and contains error.
    func provideQuizDetails(withId id: String, completion: @escaping (ResultType<QuizDetails>) -> Void) {
        Request(query: QuizQuery(id: id)).invoke { result in
            switch result {
            case .success(let quiz):
                do {
                    let quizDetails = try QuizDetailsMapper().mapToQuizDetails(json: quiz)
                    completion(ResultType.success(quizDetails))
                } catch let error {
                    completion(ResultType.failure(error))
                }
            case .failure(let error): completion(ResultType.failure(error))
            }
        }
    }
}
