//
//  QuizProvider.swift
//  Quizer
//
//  Created by Peter Bruz on 04/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import Foundation

final class QuizProvider {

    fileprivate lazy var apiQuizProvider = APIQuizProvider()
    fileprivate lazy var databaseQuizProvider = DatabaseQuizProvider()

    /// Provides list of quizzes.
    ///
    /// - Parameter completion: Closure that is called after fetching quizzes.
    ///                         If providing succeeds, ResultType is `success` and contains list of objects,
    ///                         otherwise it's `failed` and contains error.
    func provideQuizDescriptions(completion: @escaping (ResultType<[QuizDescription]>) -> Void) {
        apiQuizProvider.provideQuizDescriptions(completion: completion)
    }

    /// Provides details of quiz with given identifier.
    ///
    /// - Parameters:
    ///   - id: Identifier of quiz to provide detais for.
    ///   - completion: Closure that is called after fetching quiz details.
    ///                 If providing succeeds, ResultType is `success` and contains objects,
    ///                 otherwise it's `failed` and contains error.
    func provideQuizDetails(withId id: String, completion: @escaping (ResultType<QuizDetails>) -> Void) {
        apiQuizProvider.provideQuizDetails(withId: id, completion: completion)
    }

    /// Provides list of quizzes' statuses.
    ///
    /// - Parameter completion: Closure that is called after fetching quizzes` statuses.
    ///                         If providing succeeds, ResultType is `success` and contains list of objects,
    ///                         otherwise it's `failed` and contains error.
    func provideQuizStatusList(completion: (ResultType<[QuizStatus]>) -> Void) {
        databaseQuizProvider.provideQuizStatusList(completion: completion)
    }
}
