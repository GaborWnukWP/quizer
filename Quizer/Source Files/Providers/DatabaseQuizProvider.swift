//
//  DatabaseQuizProvider.swift
//  Quizer
//
//  Created by Peter Bruz on 05/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import Foundation

final class DatabaseQuizProvider {

    private lazy var managedQuizStatusProvider = ManagedQuizStatusProvider()

    /// Provides quizzes' statuses from database.
    ///
    /// - Parameter completion: Closure that is called after fetching quizzes' statuses from database.
    ///                         If providing succeeds, ResultType is `success` and contains list of objects,
    ///                         otherwise it's `failed` and contains error.
    func provideQuizStatusList(completion: (ResultType<[QuizStatus]>) -> Void) {
        do {
            let managedQuizStatusList = try managedQuizStatusProvider.managedQuizStatusList()
            completion(ResultType.success(managedQuizStatusList.map { $0.quizStatus }))
        } catch let error {
            completion(ResultType.failure(error))
        }
    }
}
