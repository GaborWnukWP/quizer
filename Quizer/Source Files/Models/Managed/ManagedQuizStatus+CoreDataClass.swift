//
//  ManagedQuizStatus+CoreDataClass.swift
//  
//
//  Created by Peter Bruz on 05/12/2016.
//
//

import Foundation
import CoreData

public class ManagedQuizStatus: NSManagedObject {

    class var entityName: String {
        return String(describing: ManagedQuizStatus.self)
    }

    var quizStatus: QuizStatus {
        return QuizStatus(
            quizId: mngd_quizIdentifier,
            lastQuestionOrder: Int(mngd_lastQuestionOrder),
            percentageProgress: mngd_percentageProgress,
            lastResult: mngd_lastResult,
            lastPercentageResult: mngd_lastPercentageResult
        )
    }
}
