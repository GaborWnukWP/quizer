//
//  Rate.swift
//  Quizer
//
//  Created by Peter Bruz on 03/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import Foundation
import SwiftyJSON

/// Struct that describes rate.
struct Rate {

    /// Rate's lower limit.
    let from: Int

    /// Rate's higher limit.
    let to: Int

    /// Content to display for the rate.
    let content: String

    /// Use this function to check if your result is in scope of this rate.
    ///
    /// - Parameter result: Result to check.
    /// - Returns: `True` if result is in scope of the rate. Otherwise returns `false`.
    func concerns(result: Int) -> Bool {
        return  result <= to && (result == 0 ? result >= from : result > from)
    }
}

extension Rate: Equatable {
    static func == (lhs: Rate, rhs: Rate) -> Bool {
        return lhs.from == rhs.from && lhs.to == rhs.to && lhs.content == rhs.content
    }
}

struct RateMapper {

    /// Maps json to rates' list.
    ///
    /// - Parameter json: Json to map.
    /// - Returns: Returns list of rates.
    /// - Throws: Error while mapping fails
    func mapToRateList(json: JSON?) throws -> [Rate] {

        guard let objectList = json?.array else {
            throw ObjectMappingError.failedToMapList(ofObjects: String(describing: Rate.self))
        }

        var rateList: [Rate] = []

        objectList.forEach { object in

            guard let rate = try? mapToRate(json: object) else { return }

            rateList.append(rate)
        }
        return rateList
    }

    /// Maps json to rate.
    ///
    /// - Parameter json: Json to map.
    /// - Returns: Rate.
    /// - Throws: Error while mapping fails.
    func mapToRate(json: JSON) throws -> Rate {

        guard
            let from = json[Keys.from.rawValue].int,
            let to = json[Keys.to.rawValue].int,
            let content = json[Keys.content.rawValue].string
        else {
            throw ObjectMappingError.failedToMap(object: String(describing: Rate.self))
        }

        return Rate(from: from, to: to, content: content)
    }

    enum Keys: String {
        case from = "from"
        case to = "to"
        case content = "content"
    }
}
