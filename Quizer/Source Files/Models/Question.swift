//
//  Question.swift
//  Quizer
//
//  Created by Peter Bruz on 03/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import Foundation
import SwiftyJSON

/// Struct that descibes question.
struct Question {

    /// Question's text.
    let text: String

    /// Questions order in scope of quiz.
    let order: Int

    /// Question's type.
    let type: QuestionType

    /// Answer's type.
    let answerType: AnswerType

    /// Url of question's image.
    let imageUrl: URL?

    /// List of possible answers for the question.
    let answers: [Answer]
}

/// Question's type.
///
/// - text: Represents type `QUESTION_TEXT` from API.
/// - textImage: Represents type `QUESTION_TEXT_IMAGE` from API.
enum QuestionType: String {
    case text = "QUESTION_TEXT"
    case textImage = "QUESTION_TEXT_IMAGE"
}

/// Answer's type
///
/// - text: Represents type `ANSWER_TEXT` from API.
enum AnswerType: String {
    case text = "ANSWER_TEXT"
}

struct QuestionMapper {

    /// Maps json to quiestions' list.
    ///
    /// - Parameter json: Json to map.
    /// - Returns: Returns list of questions.
    /// - Throws: Error while mapping fails.
    func mapToQuestionList(json: JSON?) throws -> [Question] {

        guard let objectList = json?.array else {
            throw ObjectMappingError.failedToMapList(ofObjects: String(describing: Question.self))
        }

        var questionList: [Question] = []

        objectList.forEach { object in

            guard let question = try? mapToQuestion(json: object) else { return }

            questionList.append(question)
        }
        return questionList
    }

    /// Maps json to question.
    ///
    /// - Parameter json: Json to map.
    /// - Returns: Question
    /// - Throws: Error while mapping fails.
    func mapToQuestion(json: JSON) throws -> Question {

        guard
            let text = json[Keys.text.rawValue].string,
            let order = json[Keys.order.rawValue].int,
            let typeString = json[Keys.type.rawValue].string,
            let type = QuestionType(rawValue: typeString),
            let answerTypeString = json[Keys.answer.rawValue].string,
            let answerType = AnswerType(rawValue: answerTypeString)
        else {
            throw ObjectMappingError.failedToMap(object: String(describing: Question.self))
        }
        var imageUrl: URL? = nil
        if type == QuestionType.textImage {
            guard let url = json[Keys.image.rawValue].dictionary?[Keys.url.rawValue]?.URL else {
                throw ObjectMappingError.failedToMap(object: String(describing: URL.self))
            }
            imageUrl = url
        }

        guard let answers = try? AnswerMapper().mapToAnswerList(json: json[Keys.answers.rawValue]) else {
            throw ObjectMappingError.failedToMap(object: String(describing: Answer.self))
        }

        return Question(
            text: text,
            order: order,
            type: type,
            answerType: answerType,
            imageUrl: imageUrl,
            answers: answers.sorted { $0.order < $1.order }
        )

    }

    enum Keys: String {
        case text = "text"
        case order = "order"
        case type = "type"
        case answer = "answer"
        case image = "image"
        case url = "url"
        case answers = "answers"
    }
}
