//
//  QuizDescription.swift
//  Quizer
//
//  Created by Peter Bruz on 03/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import Foundation
import SwiftyJSON

/// Struct to decribe basic information of quiz.
struct QuizDescription {

    /// Quiz's identifier
    let id: String

    /// Quiz's title
    let title: String

    /// Quiz's type
    let type: String

    /// Quiz's content
    let content: String

    /// Quiz's photo url.
    let photoUrl: URL?

    /// Quiz's category.
    let category: Category

    /// Quiz's status. Holds progress and last result.
    var quizStatus: QuizStatus?
}

struct QuizDescriptionMapper {

    /// Maps json to list of quizes.
    ///
    /// - Parameter json: Json to map.
    /// - Returns: Returns list of quizes.
    /// - Throws: Error while mapping fails.
    func mapToQuizDescriptionList(json: JSON?) throws -> [QuizDescription] {

        guard let objectList = json?[Keys.items.rawValue].array else {
            throw ObjectMappingError.failedToMapList(ofObjects: String(describing: QuizDescription.self))
        }

        var quizDescriptionList: [QuizDescription] = []

        objectList.forEach { object in

            guard let quizDescription = try? mapToQuizDescription(json: object) else { return }

            quizDescriptionList.append(quizDescription)
        }
        return quizDescriptionList
    }

    /// Maps json to quiz description object.
    ///
    /// - Parameter json: Json to map.
    /// - Returns: Quiz description.
    /// - Throws: Error while mapping fails.
    func mapToQuizDescription(json: JSON) throws -> QuizDescription {

        guard
            let id = json[Keys.id.rawValue].double,
            let title = json[Keys.title.rawValue].string,
            let type = json[Keys.type.rawValue].string,
            let content = json[Keys.content.rawValue].string
        else {
            throw ObjectMappingError.failedToMap(object: String(describing: QuizDescription.self))
        }

        guard let category = try? CategoryMapper().mapToCategory(json: json[Keys.category.rawValue]) else {
            throw ObjectMappingError.failedToMap(object: String(describing: Category.self))
        }

        return QuizDescription(
            id: String(format:"%.0f", id),
            title: title,
            type: type,
            content: content,
            photoUrl: json[Keys.mainPhoto.rawValue].dictionary?[Keys.url.rawValue]?.URL,
            category: category,
            quizStatus: nil
        )
    }

    enum Keys: String {
        case items = "items"
        case id = "id"
        case type = "type"
        case title = "title"
        case content = "content"
        case mainPhoto = "mainPhoto"
        case url = "url"
        case category = "category"
    }
}
