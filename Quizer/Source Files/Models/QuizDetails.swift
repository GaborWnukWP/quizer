//
//  QuizDetails.swift
//  Quizer
//
//  Created by Peter Bruz on 03/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import Foundation
import SwiftyJSON

/// Struct that describes details of quiz. Contains questions.
struct QuizDetails {

    /// Rates related to quiz.
    let rates: [Rate]

    /// Questions for quiz.
    let questions: [Question]
}

struct QuizDetailsMapper {

    /// Maps json to list of quiz details objects.
    ///
    /// - Parameter json: Json to map.
    /// - Returns: Returns list of quiz details.
    /// - Throws: Error while mapping fails.
    func mapToQuizDetails(json: JSON?) throws -> QuizDetails {

        guard
            let dictionary = json?.dictionary,
            let ratesJson = dictionary[Keys.rates.rawValue],
            let questionsJson = dictionary[Keys.questions.rawValue]
        else {
            throw ObjectMappingError.failedToMap(object: String(describing: QuizDetailsMapper.self))
        }

        guard let rates = try? RateMapper().mapToRateList(json: ratesJson) else {
            throw ObjectMappingError.failedToMap(object: String(describing: Rate.self))
        }

        guard let questions = try? QuestionMapper().mapToQuestionList(json: questionsJson) else {
            throw ObjectMappingError.failedToMap(object: String(describing: Question.self))
        }


        return QuizDetails(rates: rates, questions: questions.sorted { $0.order < $1.order })
    }

    enum Keys: String {
        case rates = "rates"
        case questions = "questions"
    }
}
