//
//  Category.swift
//  Quizer
//
//  Created by Peter Bruz on 03/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import Foundation
import SwiftyJSON

/// Struct that describes category.
struct Category {

    /// Category's identifier.
    let id: Int

    /// Category's name.
    let name: String
}

struct CategoryMapper {

    /// Maps json to category object.
    ///
    /// - Parameter json: Json to map.
    /// - Returns: Returns cateogry.
    /// - Throws: Error while mapping fails.
    func mapToCategory(json: JSON) throws -> Category {

        guard
            let id = json[Keys.id.rawValue].int,
            let name = json[Keys.name.rawValue].string
        else {
            throw ObjectMappingError.failedToMap(object: String(describing: Category.self))
        }

        return Category(id: id, name: name)
    }

    enum Keys: String {
        case id = "id"
        case name = "name"
    }
}
