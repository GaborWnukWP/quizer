//
//  Answer.swift
//  Quizer
//
//  Created by Peter Bruz on 03/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import Foundation
import SwiftyJSON

/// Struct that describes answer.
struct Answer {

    /// Answer's text.
    let text: String

    /// Answer's order in scope of question.
    let order: Int

    /// Url for answer's image.
    let imageUrl: URL?

    /// Indicates if the anser is the correct one.
    let isCorrect: Bool
}

extension Answer: Equatable {
    static func == (lhs: Answer, rhs: Answer) -> Bool {
        return lhs.text == rhs.text && lhs.order == rhs.order && lhs.imageUrl == rhs.imageUrl && lhs.isCorrect == rhs.isCorrect
    }
}

struct AnswerMapper {

    /// Maps json to answers' list.
    ///
    /// - Parameter json: Json to map.
    /// - Returns: Returns list of answers.
    /// - Throws: Error while mapping fails.
    func mapToAnswerList(json: JSON?) throws -> [Answer] {

        guard let objectList = json?.array else {
            throw ObjectMappingError.failedToMapList(ofObjects: String(describing: Answer.self))
        }

        var answerList: [Answer] = []

        objectList.forEach { object in

            guard let answer = try? mapToAnswer(json: object) else { return }

            answerList.append(answer)
        }
        return answerList
    }

    /// Maps json to answer.
    ///
    /// - Parameter json: Json to map.
    /// - Returns: Answer.
    /// - Throws: Error while mapping fails.
    func mapToAnswer(json: JSON) throws -> Answer {

        guard
            let order = json[Keys.order.rawValue].int
        else {
            throw ObjectMappingError.failedToMap(object: String(describing: Answer.self))
        }

        var text = ""

        if let textString = json[Keys.text.rawValue].string {
            text = textString
        } else if let textInt = json[Keys.text.rawValue].int {
            text = String(textInt)
        } else {
            throw ObjectMappingError.failedToMap(object: String(describing: Answer.self))
        }

        var isCorrect = false
        if let correct = json[Keys.isCorrect.rawValue].int {
            isCorrect = correct == 1
        }

        return Answer(
            text: text,
            order: order,
            imageUrl: json[Keys.image.rawValue].dictionary?[Keys.url.rawValue]?.URL,
            isCorrect: isCorrect
        )
    }

    enum Keys: String {
        case text = "text"
        case order = "order"
        case isCorrect = "isCorrect"
        case image = "image"
        case url = "url"
    }
}
