//
//  RateMapperTest.swift
//  Quizer
//
//  Created by Peter Bruz on 06/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import XCTest
import SwiftyJSON
@testable import Quizer

class RateMapperTest: XCTestCase {

    var mapper = RateMapper()
    var expectedRate: Rate?

    override func setUp() {
        super.setUp()

        expectedRate = rateMock
    }

    func testMapRate() {

        guard
            let filepath = Bundle(for: type(of: self)).path(forResource: "rate", ofType:"json"),
            let fileContent = try? String(contentsOfFile: filepath)
        else {
            XCTAssert(false)
            return
        }

        let json = JSON.parse(fileContent)

        guard let rate = try? mapper.mapToRate(json: json) else {
            XCTAssert(false)
            return
        }

        guard let expectedRate = expectedRate else {
            XCTAssert(false)
            return
        }

        XCTAssert(rate == expectedRate)
    }
}

private extension RateMapperTest {

    var rateMock: Rate {
        return Rate(from: 0, to: 20, content: "content")
    }
}
