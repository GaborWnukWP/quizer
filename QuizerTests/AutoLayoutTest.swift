//
//  AutoLayoutTest.swift
//  QuizerTests
//
//  Created by Peter Bruz on 03/12/2016.
//  Copyright © 2016 Peter Bruz. All rights reserved.
//

import XCTest
@testable import Quizer

class AutoLayoutTest: XCTestCase {

    func testUIHasAmbiguousLayout() {
        guard let window =  UIApplication.shared.delegate?.window! else {
            XCTAssert(false)
            return
        }

        /// Note!
        /// If this test fails for you, here is what you want to do:
        /// 1. Set breakpoint on XCTAssert line
        /// 2. When app execution stops on breakpoint...
        /// 3. Paste this into terminal: `expr -l objc++ -o -- [[UIWindow keyWindow] _autolayoutTrace]`
        /// 4. System will check whole views hierarchy and mark views with ambiguous layout.
        XCTAssert(window.hasAmbiguousLayout == false)
    }
}
